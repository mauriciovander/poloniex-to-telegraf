FROM node:carbon

COPY . .

RUN npm install --save
RUN ./node_modules/.bin/tsc index.ts

CMD node index.js